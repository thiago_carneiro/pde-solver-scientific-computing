function solutions = disp_tables(method,N, b_vectors)
    l = size(N,1);
    runtime=zeros(l,1);
    storage=zeros(l,1);
    solutions = cell(l,1);
    surface_f = figure('Name', method + " Surface Plots");
    contour_f = figure('Name', method + " Contour Plots");
    for i=1:l
        b=b_vectors{i};
        if method=="Gauss-Seidel"
            f=@() gauss_seidel(b,N(i,1),N(i,2));
            T=gauss_seidel(b,N(i,1),N(i,2));
            solutions{i} = T;
            runtime(i)=timeit(f);
            storage(i)=(N(i,1)+2)*(N(i,2)+2)+N(i,1)*N(i,2);
        elseif method=="Full Matrix"
            A=generate_dense_heat_matrix(N(i,1),N(i,2));
            f=@() solve_matrix(A,b,N(i,1),N(i,2));
            T=solve_matrix(A,b,N(i,1),N(i,2));
            solutions{i} = T;
            runtime(i)=timeit(f);
            storage(i)=2*N(i,1)*N(i,2)+(N(i,1)*N(i,2))^2;
        else
            A=gen_sparse_matrix(N(i,1),N(i,2));
            f=@() solve_matrix(A,b,N(i,1),N(i,2));
            T=solve_matrix(A,b,N(i,1),N(i,2));
            solutions{i} = T;
            runtime(i)=timeit(f);
            storage(i)=nnz(A)+2*N(i,1)*N(i,2);
        end
         figure(surface_f);
         plot_temp(T,N(i,1),N(i,2),i,'S');
         title(strcat(method, strcat(" Nx=",num2str(N(i,1))," ,Ny=",num2str(N(i,2)))));
         figure(contour_f);
         plot_temp(T,N(i,1),N(i,2),i,'C');
         title(strcat(method, strcat(" Nx=",num2str(N(i,1))," ,Ny=",num2str(N(i,2)))));
    end

    T=table(N(:,1),N(:,2),runtime,storage,'VariableNames',{'Nx','Ny','Runtime','Storage'});
    disp(table(T,'VariableNames',{method}));

end

