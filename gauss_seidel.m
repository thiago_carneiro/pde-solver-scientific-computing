function grid = gauss_seidel(b,Nx,Ny)
%%function that perform the Gauss-Seidel method
%             b = b_vector (from Ax=b)

grid = zeros(Ny+2,Nx+2);
hx_2_inv = (Nx+1)*(Nx+1);
hy_2_inv = (Ny+1)*(Ny+1);
point_coeffecient = 1/(-2*(hx_2_inv+hy_2_inv));
center_coef = -2*(hx_2_inv+hy_2_inv);
residual_norm = inf;

while residual_norm > 10^-4
    b_index = 1;
    for j=2:Ny+1
        for i=2:Nx+1
            grid(j,i) = (b(b_index) - (grid(j,i-1)+grid(j,i+1))*hx_2_inv - (grid(j-1,i)+grid(j+1,i))*hy_2_inv)*point_coeffecient;
            b_index = b_index + 1;
        end
    end
    residual_sub_sum = 0;
    b_index = 1;
    for j=2:Ny+1
        for i=2:Nx+1
            residual_sub_sum = residual_sub_sum + (b(b_index)-grid(j,i)*center_coef-(grid(j,i-1)+grid(j,i+1))*hx_2_inv-(grid(j-1,i)+grid(j+1,i))*hy_2_inv)^2;
            b_index = b_index + 1;
        end
    end
    residual_norm = sqrt((1/(Nx*Ny))*residual_sub_sum);
end
grid = grid(2:Ny+1,2:Nx+1);
end