function plot_temp(T,Nx,Ny,i,plot_type)
    hx=1/(Nx+1);
    hy=1/(Ny+1);
    [X,Y]=meshgrid(0:hx:1,0:hy:1);
    Z=zeros(Ny+2,Nx+2);
    Z(2:Ny+1,2:Nx+1) = T;
    subplot(2,3,i);
    if(plot_type=='S')
        surf(X,Y,Z,'FaceColor','interp');
    else
        contourf(X,Y,Z);
        colorbar('westoutside');
    end
    xlabel('x');
    ylabel('y');
    zlabel('T');
end