function error_gauss_seidel(T, N)
    %%function that calculates the error for the Gauss-Seidel method for
    %%the cases Nx = Ny = 7; 15; 31; 63; 127.

    l = size(N,1);
    errors = zeros(l,1);
    error_reduction = zeros(l,1);
    for i=1:l
        errors(i) = calc_error(T{i},N(i,1),N(i,2));
        if i == 1
            error_reduction(i) = nan;
        else
            error_reduction(i) = errors(i-1)/errors(i);
        end
    end
    T=table(N(:,1),N(:,2),errors,error_reduction,'VariableNames',{'Nx','Ny','error','error_reduction'});
    disp(table(T,'VariableNames',{'Gauss-Seidel error compared to exact solution'}));
end