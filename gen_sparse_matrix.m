function A=gen_sparse_matrix(Nx,Ny)
    %System Parameters
    N=Nx*Ny;
    hx=1/(Nx+1);
    hy=1/(Ny+1);
    h=-2*(hx^2+hy^2)/(hx*hy)^2;
    base=(1/hx^2)*ones(Nx-1,1);

    %Diagonals
    subdiag1=[base ; 0];
    diag1=[0 ; base];
    diag1=repmat(diag1,Ny,1);
    subdiag1=repmat(subdiag1,Ny,1);
    diag5=ones(N,1)/hy^2;
    mdiag=h*ones(N,1);
    
    d=[-Nx -1 0 1 Nx];
    
    B=[diag5 subdiag1 mdiag diag1 diag5];
    A=spdiags(B,d,N,N);
end