function b = gen_b(Nx,Ny,rh_func,boundary_func)
%%function that generates b given the inputs:
%   Nx = size of the grid in the x dimension;
%   Ny = size of the grid in the y dimension;
%   rh_func = right hand side function of the PDE; 
%   boundary_func = boundary_condition_func(x,y);
hx = 1/(Nx+1);
hy = 1/(Ny+1);
hx_2_inv = (Nx+1)^2;
hy_2_inv = (Ny+1)^2;
b_size = Nx*Ny;
b = zeros(b_size,1);
if b_size > 0
    b(1) = rh_func(hx,hy) - boundary_func(0,hy)*hx_2_inv - boundary_func(hx,0)*hy_2_inv;
    for k=2:Nx-1
        i = mod(k-1,Nx) + 1;
        j = floor((k-1)/Nx) + 1;
        b(k) = rh_func(i*hx,j*hy) - boundary_func(i*hx,0)*hy_2_inv;
    end
    b(Nx) = rh_func(Nx*hx,hy) - boundary_func(1,hy)*hx_2_inv - boundary_func(Nx*hx,0)*hy_2_inv;
    for j=2:Ny-1
        b(1+j*Nx) = rh_func(i*hx,j*hy) - boundary_func(0,j*hy)*hx_2_inv;
        for i=2:Nx-1
            b(i+j*Nx) = rh_func(i*hx,j*hy);
        end
        b(Nx+j*Nx) = rh_func(i*hx,j*hy) - boundary_func(1,j*hy)*hx_2_inv;
    end
    for k=1:b_size
        i = mod(k-1,Nx) + 1;
        j = floor((k-1)/Nx) + 1;
        b(k) = -2 * pi^2 * sin(pi*i*hx) * sin(pi*j*hy);
    end
    b(b_size - Nx + 1) = rh_func(hx,Ny*hy) - boundary_func(0,Ny*hy)*hx_2_inv - boundary_func(hx,1)*hy_2_inv;
    for k=b_size-Nx+2:b_size-1
        i = mod(k-1,Nx) + 1;
        j = floor((k-1)/Nx) + 1;
        b(k) = rh_func(i*hx,j*hy) - boundary_func(i*hx,1)*hy_2_inv;
    end
    b(b_size) = rh_func(Nx*hx,Ny*hy) - boundary_func(1,Ny*hy)*hx_2_inv - boundary_func(Nx*hx,1)*hy_2_inv;
end
end