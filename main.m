clear all;
clc;

N = [3,3;7,7;15,15;31,31;63,63]; %First column represnts Nx, second for Ny
l = size(N,1);



right_hand_side_func = @(x,y) -2*pi^2*sin(pi*x)*sin(pi*y);

%% boundary_condition_func = @(x,y) sets Dirichlet BC and allows to give as BC
% either a function f(x,y) or constant values.
% Set homogeneous BC (0 for all sides)

boundary_condition_func = @(x,y) 0;

% function to generate the vector b (Ax = b) given the inputs: Nx, Ny,
% right_hand_side_func, boundary_condition_func.

b_vectors = cell(l,1);
for i=1:l
    b_vectors{i} = generate_b(N(i,1),N(i,2),right_hand_side_func,boundary_condition_func);
end

%% disp_tables(method, N, b_vector) generates the graphs and tables for all the methods (Full Matrix, SparseMatrix, Gauss-Seidel)
%   used to solve the linear system of equations Ax = b, for Nx = Ny = 3,7,15,31,63

disp_tables('Full Matrix',N, b_vectors); % solve Ax = b by MATLAB direct solver for the cases Nx = Ny = 3,7,15,31,63
disp_tables('Sparse Matrix',N, b_vectors); % solve Ax = b by MATLAB direct solver using a sparse matrix A for the cases Nx = Ny = 3,7,15,31,63
T=disp_tables('Gauss-Seidel',N, b_vectors); % solve Ax = b by Gauss-Seidel method for the cases Nx = Ny = 3,7,15,31,63

%% Compute the solutions for Nx = Ny = 7; 15; 31; 63; 127 with the Gauss-Seidel solver
%  and the resulting error of the respective Nx = Ny.

Nx = 127;
Ny=127;
b_127 = generate_b(Nx,Ny,right_hand_side_func,boundary_condition_func);
T{end+1} = gauss_seidel(b_127,Nx,Ny);
T=T(2:end);

N(end+1,:) = [Nx,Ny];
N = N(2:end,:);

error_gauss_seidel(T, N);