function T_grid = solve_matrix(A,b,Nx,Ny)
    T_grid = reshape(A\b,Nx,Ny)';
end