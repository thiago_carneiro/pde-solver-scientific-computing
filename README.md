# PDE-Solver for Steady-State Heat Equation


```
Sketch of the domain

  __________________
 |                  |    ^
 |                  |    |
 |      plate       |    N_x
 |                  |    |
 |__________________|    v

 <------- N_y ------>
  
```

