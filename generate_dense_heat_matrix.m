function heat_mat = generate_dense_heat_matrix(Nx, Ny)
hx_2 = (1/(Nx+1))^2;
hy_2 = (1/(Ny+1))^2;
grid_size = Nx * Ny;
heat_mat = zeros(grid_size);
for node=1:grid_size
    heat_mat(node,node) = -2/hx_2 -2/hy_2;
    if mod(node, Nx) ~= 0
        heat_mat(node,node+1) = 1/hx_2;
    end
    if mod(node,Nx) ~= 1
        heat_mat(node,node-1) = 1/hx_2;
    end
    if node > Nx
        heat_mat(node,node-Nx) = 1/hy_2;
    end
    if node <= grid_size - Nx
        heat_mat(node,node+Nx) = 1/hy_2;
    end
end